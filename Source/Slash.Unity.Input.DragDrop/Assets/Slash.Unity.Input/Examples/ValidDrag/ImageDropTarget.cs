﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageDropTarget.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.Input.Examples.ValidDrag
{
    using Slash.Unity.Input.DragDrop;

    using UnityEngine;
    using UnityEngine.UI;

    public class ImageDropTarget : MonoBehaviour
    {
        #region Fields

        public Image Image;

        #endregion

        #region Public Methods and Operators

        public void OnDragOver(DragDropOperation operation)
        {
            // Check if image data.
            var imageData = operation.Data as ImageDragData;
            operation.DropSuccessful = imageData != null;
        }

        public void OnDrop(DragDropOperation operation)
        {
            // Check if image data.
            var imageData = operation.Data as ImageDragData;
            if (imageData == null)
            {
                return;
            }

            // Perform drop.
            if (this.Image != null)
            {
                this.Image.sprite = imageData.Sprite;
            }
        }

        protected void Reset()
        {
            if (this.Image == null)
            {
                this.Image = this.GetComponent<Image>();
            }
        }
        #endregion
    }
}