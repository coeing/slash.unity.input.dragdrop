﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageDragData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.Input.Examples.ValidDrag
{
    using UnityEngine;

    public class ImageDragData
    {
        #region Properties

        public Sprite Sprite { get; set; }

        #endregion
    }
}