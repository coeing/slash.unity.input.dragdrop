﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageDragDataGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.Input.Examples.ValidDrag
{
    using Slash.Unity.Input.DragDrop;

    using UnityEngine;
    using UnityEngine.UI;

    public class ImageDragDataGetter : MonoBehaviour
    {
        #region Fields

        public Image Image;

        #endregion

        #region Public Methods and Operators

        public void GetDragData(DragDropOperation operation)
        {
            operation.Data = new ImageDragData() { Sprite = this.Image != null ? this.Image.sprite : null };
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.Image == null)
            {
                this.Image = this.GetComponent<Image>();
            }
        }

        #endregion
    }
}