﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Drag3DData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;

namespace Slash.Unity.Input.Examples.Drag3D
{
    public class Drag3DData
    {
        public Texture Texture { get; set; }
    }
}