﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Drag3DDropTarget.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Slash.Unity.Input.DragDrop;
using UnityEngine;

namespace Slash.Unity.Input.Examples.Drag3D
{
    public class Drag3DDropTarget : MonoBehaviour
    {
        public Renderer Renderer;

        public void OnDragOver(DragDropOperation operation)
        {
            // Check if image data.
            var dragData = operation.Data as Drag3DData;
            operation.DropSuccessful = dragData != null;
        }

        public void OnDrop(DragDropOperation operation)
        {
            // Check if image data.
            var dragData = operation.Data as Drag3DData;
            if (dragData == null)
            {
                return;
            }

            // Perform drop.
            if (this.Renderer != null)
            {
                this.Renderer.material.mainTexture = dragData.Texture;
            }
        }

        protected void Reset()
        {
            if (this.Renderer == null)
            {
                this.Renderer = this.GetComponent<Renderer>();
            }
        }
    }
}