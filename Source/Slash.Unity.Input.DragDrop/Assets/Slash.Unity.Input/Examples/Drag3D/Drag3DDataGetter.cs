﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageDragDataGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Slash.Unity.Input.DragDrop;
using UnityEngine;

namespace Slash.Unity.Input.Examples.Drag3D
{
    public class Drag3DDataGetter : MonoBehaviour
    {
        public Texture Texture;

        public void GetDragData(DragDropOperation operation)
        {
            operation.Data = new Drag3DData {Texture = this.Texture };
        }
    }
}