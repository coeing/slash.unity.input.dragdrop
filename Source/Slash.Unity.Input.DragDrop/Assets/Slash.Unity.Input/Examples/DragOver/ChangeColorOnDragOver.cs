﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangeColorOnDragOver.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.Input.Examples.DragOver
{
    using Slash.Unity.Input.DragDrop;

    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ChangeColorOnDragOver : MonoBehaviour, IDragOverHandler, IPointerExitHandler
    {
        #region Fields

        public Color DragOverColor;

        public Graphic Graphic;

        private Color initialColor;

        #endregion

        #region Public Methods and Operators

        public void OnDragOver(PointerEventData eventData)
        {
            if (this.Graphic != null)
            {
                this.Graphic.color = this.DragOverColor;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (this.Graphic != null)
            {
                this.Graphic.color = this.initialColor;
            }
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.Graphic == null)
            {
                this.Graphic = this.GetComponent<Graphic>();
            }
        }

        protected void Start()
        {
            if (this.Graphic != null)
            {
                this.initialColor = this.Graphic.color;
            }
        }

        #endregion
    }
}