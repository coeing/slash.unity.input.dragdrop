﻿using UnityEngine;

namespace Slash.Unity.Input.DragDrop
{
    public class IgnoreRaycast : MonoBehaviour
    {
        public int IgnoreRaycastLayer = 2;

        private int initialLayer;

        private void OnDisable()
        {
            this.gameObject.layer = this.initialLayer;
        }

        private void OnEnable()
        {
            this.initialLayer = this.gameObject.layer;
            this.gameObject.layer = this.IgnoreRaycastLayer;
        }
    }
}